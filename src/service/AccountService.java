package service;

import dao.AccountDAo;
import dao.AccountDaoImp;
import domain.Account;
import domain.Transaction;
import java. util. List;

import java.math.BigDecimal;
import java.sql.SQLException;

import static java.math.BigDecimal.ZERO;


/**
 * Created by user on 30.11.2016.
 */
public class AccountService {
    private AccountDAo aDao = new AccountDaoImp();
    private TransactionService tService = new TransactionService();

    public Account get (long id) {
        return aDao.get(id);
    }
    public long save (Account account)throws SQLException{
        Long accountId = aDao.save(account);
        if (!account.getBalance().equals(ZERO)){
            tService.save(new Transaction(null, account.getBalance(), account.getCreationdate(), "PUT", accountId));
        }
        return accountId;

    }
    public void update(Account account)throws SQLException{
        aDao.update(account);

    }


    public void delete(Account account) throws SQLException {
        tService.deleteByAccount(account.getAccountnumber());
        aDao.delete(account);
    }

    public void deleteByCustomer(Long customerid) {

    }
}
