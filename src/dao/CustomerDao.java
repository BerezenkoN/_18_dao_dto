package dao;

import domain.Customer;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by user on 28.11.2016.
 */
public interface CustomerDao {

    Customer get(Long id);

    Long save(Customer customer) throws SQLException;

    void update (Customer customer) throws SQLException;

    void delete(Customer customer) throws SQLException;

    List<Customer> list();

    }
