package dao;

import domain.Transaction;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by user on 28.11.2016.
 */
public interface TransactionDao {

    Transaction get(Long id);

    Long save(Transaction transaction) throws SQLException;

    void update (Transaction transaction) throws SQLException;

    void delete(Transaction transaction) throws SQLException;

    List<Transaction> list();

    void deleteByAccount(Long accountId) throws SQLException;
}

