package dao;

import domain.Account;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 28.11.2016.
 */
public class AccountDaoImp implements AccountDAo {
    @Override
    public Account get(Long accountnumber) {
        try (Connection conn = ConnectionUtil.createConnection()) {
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement("SELECT * FROM accounts WHERE accountnumber = ?");
                ps.setLong(1, accountnumber);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    return fromAccount(rs);
                }
                rs.close();
                ps.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Long save(Account account) throws SQLException {
        try (Connection conn = ConnectionUtil.createConnection()) {
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement("INSER INTO accounts (balance, creationdate,currency, blocked, customerid) VALUES (?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
                ps.setBigDecimal(1, account.getBalance());
                ps.setTimestamp(2, account.getCreationdate());
                ps.setString(3, account.getCurrency());
                ps.setBoolean(4, account.getBlocked());
                ps.setLong(5, account.getCustomerid());
                ps.close();

                int affectedRows = ps.executeUpdate();

                if (affectedRows == 0) {
                    throw new SQLException("Creating account failed, no rows affected.");
                }
                try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getLong(1);
                    } else {
                        throw new SQLException("Creating account failed, no ID obtained.");
                    }



                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    @Override
    public List<Account> list() {
        try (Connection conn = ConnectionUtil.createConnection()) {
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement("SELECT * FROM accounts");
                ResultSet rs = ps.executeQuery();
                List<Account> accounts = new ArrayList<Account>();
                while (rs.next()) {
                    accounts.add(fromAccount(rs));
                }
                return accounts;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void deleteByCustomer(Long customerId) throws SQLException {
        try (Connection conn = ConnectionUtil.createConnection()) {
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement("SELECT FROM accounts WHERE customerid = ?");
                ps.setLong(1, customerId);
                ResultSet rs = ps.executeQuery();
                List<Account> accounts = new ArrayList<>();
                while (rs.next()) {
                    accounts.add(fromAccount(rs));
                    ps.close();
                    rs.close();
                }
            }

        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Account account) {
        try (Connection conn = ConnectionUtil.createConnection()) {
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement("DELETE FROM accounts WHERE accountnumber = ?");
                ps.setLong(1, account.getAccountnumber());
                ps.executeUpdate();
                ps.close();
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void update (Account account) throws  SQLException {
        try (Connection conn = ConnectionUtil.createConnection()) {
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement("UPDATE accounts SET balance = ?, creationdate = ?, " +
                        "currency = ?, blocked = ?, customerid = ?, WHERE accountnumber = ?");

                ps.setBigDecimal(1, account.getBalance());
                ps.setTimestamp(2, account.getCreationdate());
                ps.setString(3, account.getCurrency());
                ps.setBoolean(4, account.getBlocked());
                ps.setLong(5, account.getCustomerid());
                ps.setLong(6, account.getAccountnumber());
                ps.executeUpdate();
                ps.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private Account fromAccount(ResultSet resultSet) throws SQLException {

            Account account = new Account(resultSet.getLong("accountnumber"),
                    resultSet.getBigDecimal("balance"),
                    resultSet.getTimestamp("creationdate"),
                    resultSet.getString("currency"),
                resultSet.getBoolean("blocked"),
                resultSet.getLong("customerid"));


        return account;

    }
}


