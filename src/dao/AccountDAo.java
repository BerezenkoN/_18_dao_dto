package dao;

import domain.Account;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by user on 28.11.2016.
 */
public interface AccountDAo {

        Account get(Long id);

        Long save(Account account) throws SQLException;

        void update (Account account)throws SQLException;

        void delete(Account account)throws SQLException;

        List<Account> list();

        void deleteByCustomer(Long customerId) throws SQLException;

    }


