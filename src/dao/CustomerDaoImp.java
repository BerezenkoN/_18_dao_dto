package dao;



import domain.Customer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.sql.SQLException;

/**
 * Created by user on 28.11.2016.
 */
public class CustomerDaoImp implements CustomerDao {


    @Override
    public Customer get(Long id) {
        try (Connection conn = ConnectionUtil.createConnection()) {
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement("SELECT * FROM customers WHERE id = ?");
                ps.setLong(1, id);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    return fromCustomer(rs);
                }
                ps.close();
                rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<Customer> list() {
        try (Connection conn = ConnectionUtil.createConnection()) {
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement("SELECT * FROM customers");
                ResultSet rs = ps.executeQuery();
                List<Customer> customers = new ArrayList<Customer>();
                while (rs.next()) {
                    customers.add(fromCustomer(rs));
                }
                return customers;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void delete(Customer customer) {
        try (Connection conn = ConnectionUtil.createConnection()) {
            if (conn != null) {
                Statement st = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

                ResultSet rs = st.executeQuery("SELECT * FROM customers");

                if (rs.first() != false) {
                    rs.beforeFirst();
                    while (!rs.isLast()) {
                        rs.next();
                        if (rs.getLong("id") == customer.getId()) {

                            rs.deleteRow();
                            break;
                        }
                        if (rs.isLast()) {
                            System.out.println("There are stacks customer ID number!");
                        }
                    }
                }
                System.out.println("Data are not true or delete!");

                rs.close();
                st.close();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Override
   public void update (Customer customer) throws  SQLException {
        try (Connection conn = ConnectionUtil.createConnection()) {
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement("UPDATE customers SET firstname = ?, lastname = ?, birthdate = ?, " +
                        "address = ?, city = ?, passport = ?, phone = ? WHERE id = ?");

                ps.setString(1, customer.getFirstname());
                ps.setString(2, customer.getLastname());
                ps.setDate(3, customer.getBirthdate());
                ps.setString(4, customer.getAddress());
                ps.setString(5, customer.getCity());
                ps.setString(6, customer.getPassport());
                ps.setString(7, customer.getPhone());
                ps.setLong(8, customer.getId());
                ps.executeUpdate();
                ps.close();
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }


            @Override
            public Long save (Customer customer)throws SQLException {
                try (Connection conn = ConnectionUtil.createConnection()) {
                    if (conn != null) {
                        PreparedStatement ps = conn.prepareStatement("INSERT INTO customers (firstname, lastname, birthdate, " +
                                "address, city, passport, phone VALUES (?,?,?,?,?,?,?))", Statement.RETURN_GENERATED_KEYS);
                        ps.setString(1, customer.getFirstname());
                        ps.setString(2, customer.getLastname());
                        ps.setDate(3, customer.getBirthdate());
                        ps.setString(4, customer.getAddress());
                        ps.setString(5, customer.getCity());
                        ps.setString(6, customer.getPassport());
                        ps.setString(7, customer.getPhone());
                        ps.close();

                        int affectedRows = ps.executeUpdate();

                        if (affectedRows == 0) {
                            throw new SQLException("Creating customer failed, no rows affected.");
                        }
                        try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                            if (generatedKeys.next()) {
                                customer.setId(generatedKeys.getLong(1));
                            } else {
                                throw new SQLException("Creating customer failed, no ID obtained.");
                            }

                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
                return null;
            }





    private Customer fromCustomer(ResultSet resultSet) throws SQLException {

        Customer customer = new Customer(resultSet.getLong("id"),
                resultSet.getString("firstname"),
                resultSet.getString("lastname"),
                resultSet.getDate("birthdate"),
                resultSet.getString("address"),
                resultSet.getString("city"),
                resultSet.getString("passport"),
                resultSet.getString("phone")
        );
        return customer;

    }
}

