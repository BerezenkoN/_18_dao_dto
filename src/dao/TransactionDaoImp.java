package dao;

import domain.Transaction;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 30.11.2016.
 */
public class TransactionDaoImp implements TransactionDao {
    @Override
    public Transaction get(Long id) {
        try (Connection conn = ConnectionUtil.createConnection()) {
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement("SELECT * FROM trsansactions WHERE id = ?");
                ps.setLong(1, id);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    return fromTransaction(rs);
                }
                ps.close();
                rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Long save(Transaction transaction) throws SQLException {
        try (Connection conn = ConnectionUtil.createConnection()) {
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement("INSERT INTO transactions (amount, date, operetiontype," +
                        " accountnumber) VALUES (?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
                ps.setBigDecimal(1, transaction.getAmount());
                ps.setTimestamp(2, transaction.getDate());
                ps.setString(3, transaction.getOperationtype());
                ps.setLong(4, transaction.getAccountnumber());
                int affectedRows = ps.executeUpdate();
                ps.close();

                if (affectedRows == 0) {
                    throw new SQLException("Creating trsnsaction failed, no rows affected.");
                }
                try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getLong(1);
                    } else {
                        throw new SQLException("Creating transaction failed, no ID obtained.");
                    }


                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }
    }

    @Override
    public void update(Transaction transaction) {
            try (Connection conn = ConnectionUtil.createConnection()) {
                if (conn != null) {
                    PreparedStatement ps = conn.prepareStatement("UPDATE transactions SET amount = ?, date = ?, operetiontype = ?,accountnumber = ? WHERE id = ?)");

                    ps.setBigDecimal(1, transaction.getAmount());
                    ps.setTimestamp(2, transaction.getDate());
                    ps.setString(3, transaction.getOperationtype());
                    ps.setLong(4, transaction.getAccountnumber());
                    ps.executeUpdate();
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }




    @Override
    public void delete(Transaction transaction) {
        try (Connection conn = ConnectionUtil.createConnection()) {
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement("DELETE FROM transactions WHERE id = ?");
                ps.setLong(1, transaction.getId());
                ps.executeUpdate();
                ps.close();
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    @Override
    public List<Transaction> list() {
        try (Connection conn = ConnectionUtil.createConnection()) {
                if (conn != null) {
                    PreparedStatement ps = conn.prepareStatement("SELECT * FROM transactions");
                    ResultSet rs = ps.executeQuery();
                    List<Transaction> transactions = new ArrayList<Transaction>();
                    while (rs.next()) {
                        transactions.add(fromTransaction(rs));
                        rs.close();
                    }
                    return transactions;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            return null;
        }



    @Override
    public void deleteByAccount(Long accountId) throws SQLException{
        try (Connection conn = ConnectionUtil.createConnection()) {
            if (conn != null) {
                PreparedStatement ps = conn.prepareStatement("DELETE FROM transactions WHERE accountnumber = ?");
                ps.setLong(1, accountId);
                ps.executeUpdate();
            }
            }catch (SQLException e) {
            e.printStackTrace();
        }

    }




    private Transaction fromTransaction (ResultSet resultSet)throws SQLException{
    return new Transaction(
            resultSet.getLong("id"),
            resultSet.getBigDecimal("amount"),
            resultSet.getTimestamp("date"),
            resultSet.getString("operationtype"),
            resultSet.getLong("accountnumber"));
    }

    }

