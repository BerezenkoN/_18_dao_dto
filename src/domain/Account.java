package domain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by user on 27.11.2016.
 */
public class Account {


    private  Long accountnumber;

    private BigDecimal balance;

    private Timestamp creationdate;

    private  String currency;

    private Boolean blocked = false;

    private Long customerid;

    public Account(Long accountnumber, BigDecimal balance, Timestamp creationdate, String currency, boolean blocked, Long customerid) {

    }


    public Long getAccountnumber() {
        return accountnumber;
    }

    public void setAccountnumber(Long accountnumber) {
        this.accountnumber = accountnumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Timestamp getCreationdate() {
        return creationdate;
    }

    public void setCreationdate(Timestamp creationdate) {
        this.creationdate = creationdate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }

    public Long getCustomerid() {
        return customerid;
    }

    public void setCustomerid(Long customerid) {
        this.customerid = customerid;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountnumber=" + accountnumber +
                ", balance=" + balance +
                ", creationdate=" + creationdate +
                ", currency='" + currency + '\'' +
                ", blocked=" + blocked +
                ", customerid=" + customerid +
                '}';
    }
}


