package domain;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by user on 27.11.2016.
 */
public class Transaction {

    private Long id;

    private BigDecimal amount;

    private Timestamp date;

    private  String operationtype;

    private Long accountnumber;

    public Transaction(Long id, BigDecimal amount, Timestamp date, String operationtype, long accountnumber) {

    }



    public Long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Timestamp getDate() {
        return date;
    }

    public String getOperationtype() {
        return operationtype;
    }

    public Long getAccountnumber() {
        return accountnumber;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public void setOperationtype(String operationtype) {
        this.operationtype = operationtype;
    }

    public void setAccountnumber(Long accountnumber) {
        this.accountnumber = accountnumber;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", amount=" + amount +
                ", date=" + date +
                ", operationtype='" + operationtype + '\'' +
                ", accountnumber=" + accountnumber +
                '}';
    }
}
